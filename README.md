## 1. PRIMEROS PASOS
### Requisitos 
Crear de una forma básica un PHP que nos permita desarrollar lo que buscamos.


### La idea 
Cómo crear una API simple basada en PHP con el marco Symfony.
Cómo crear puntos finales API GET y POST para ello.
Cómo enviar solicitudes a esos puntos finales con Postman.


### Lo Necesario
- Git Instalado (Required) ✅ ![Git](https://img.shields.io/badge/Git-F05032.svg?style=for-the-badge&logo=Git&logoColor=white)
- PHP Instalado (Required) ✅ ![PHP](https://img.shields.io/badge/PHP-777BB4.svg?style=for-the-badge&logo=PHP&logoColor=white)
    - Un lenguaje de programación interpretado​ del lado del servidor y de uso general que se adapta especialmente al desarrollo web.​
- Composer Instalado (Required) ✅ ![Composer](https://img.shields.io/badge/Composer-885630.svg?style=for-the-badge&logo=Composer&logoColor=white)
    - Un administrador de dependencias para PHP. Es una herramienta simple y confiable que los desarrolladores usan para administrar e integrar paquetes o bibliotecas externas en sus proyectos basados en PHP. De esta manera, no tienen que crear sus páginas web o aplicaciones web desde cero.
- Symfony CLI Instalado (Required) ✅ ![symfony](https://img.shields.io/badge/Symfony-000000.svg?style=for-the-badge&logo=Symfony&logoColor=white)
    - Un popular framework de PHP para desarrollo de aplicaciones web y de soporte.
- Homebrew Instalado (Optional) ✅ ![Homebrew](https://img.shields.io/badge/Homebrew-FBB040.svg?style=for-the-badge&logo=Homebrew&logoColor=black)
- VSCode Instalado (Optional) ✅ ![VisualStudio](https://img.shields.io/badge/Visual%20Studio%20Code-007ACC.svg?style=for-the-badge&logo=Visual-Studio-Code&logoColor=white)
      o un editor de codigo que tu quieras.
    - Tendremos que instalar unas extensiones para el VSC, las que he probado han sido [PHP Getters & Setters](https://marketplace.visualstudio.com/items?itemName=phproberto.vscode-php-getters-setters) y [PHP Intelephense](https://marketplace.visualstudio.com/items?itemName=bmewburn.vscode-intelephense-client) ambas nos permitiran funcionar sin problemas con PHP.
- Postman (Opcional) ✅ ![Postman](https://img.shields.io/badge/Postman-FF6C37.svg?style=for-the-badge&logo=Postman&logoColor=white)
                                                                        

---

## 2. INSTALACIONES
### Instalando PHP
Los métodos siguientes instalan PHP con todas las dependencias del módulo para ejecutar Symfony localmente. Si ya instaló PHP localmente, asegúrese de tener instalados y habilitados los módulos en los requisitos del servidor Symfony.

MacOS
PHP es más sencillo de instalar con Homebrew.

~~~ 
brew install php
~~~

Después de la instalacion, confirma PHP confirma que esta instalado :  php -v en la terminal.

Instalación [XAMPP](https://www.apachefriends.org/download.html) or [MAMP](https://www.mamp.info/en/mac/) ( en MAcOs Ventura 13.5.1 funcionan ambos perfectamente, en las nuevas versiones MacOs Sonoma a veces da algún fallo ).
### Instalando Composer
MacOS ve a [system](https://getcomposer.org/doc/00-intro.md#system-requirements).

Homebrew instalación [install](https://brew.sh/).

~~~ 
brew install composer
~~~ 
<img width="400" height="200" src="brew.png"/>
Verificar Composer
Abre una terminar y escribe type composer -h.

### Instalando Symfony CLI
Podremos hacer la instalación desde las webs [multi-platform installation instructions](https://symfony.com/download) y [Symfony Command Line Interface (CLI)](https://symfony.com/download).

## 3. Creando
### Crear un proyecto con Symfony
Comenzaremos creando un nuevo proyecto.

~~~ 
symfony new pruebaphp
~~~ 

Para verificar que todo salió correctamente durante la descarga y creación, inicia el servidor web que incluye el propio framework ejecutando:

~~~ 
symfony server:start
~~~ 

De esta forma podras acceder al enlace que te propociona el servidor y ver esta imagen en el navegador :

<img width="400" height="300" src="sym.png"/>

### Estructurar la aplicación
Después de crear la aplicación, al entrar en pruebaphp/ se puede ver la siguiente estructura de archivos y directorios ( dependerá mucho de las versión de Symfony que utilizemos ):


    ├─ assets/
    ├─ bin/
       └─ console
    ├─ config/
    ├─ public/
    ├─ src/
      └─ AppBundle/
    ├─ templates/
    ├─ tests/
       └─ AppBundle/
    ├─ transitions/
    ├─ var/
       ├─ cache/
       ├─ logs/
       └─ sessions/
    ├─ vendor/
    


Esta jerarquía es la convención propuesta por Symfony para estructurar aplicaciones. El objetivo de cada directorio es el siguiente:

- src/AppBundle/. Guarda el código específico de Symfony (controllers y routes), tu código de dominio (como clases Doctrine) y toda la business logic.
- var/cache/. Guarda los archivos cache generados por la aplicación.
- var/logs/. Guarda los archivos log generados por la aplicación.
- var/sessions/. Guarda los archivos de sesión generados por la aplicación.
- tests/AppBundle/. Guarda tests automáticos (como Unit tests) de la aplicación.
- vendor/. Directorio donde Composer instala las dependencias de la aplicación. Nunca se ha de tocar este directorio.
...

Aquí pongo una pequeña captura del proyecto creado en la terminal con la versión Symfony 6.3.7 para que podamos ver un poco todo lo que nos crea de manera automatizada.

<img width="600" height="100" src="project.png"/>

## 4. Creando un Endpoint
Tendras que ir al directorio pruebaphp y dentro de está ( todo esto se podría hacer desde un .yaml ... de forma más plana y trabajada ):

`/src/Controller/ Crea un archivo llamado HelloController.php`

Pon este codigo para comenzar a crear :

```
<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


class HelloController extends AbstractController
{
    //set the route, so [site URL]/hello will trigger this
    #[Route('/hello', name: 'hello_world')]
    public function hello(): Response
    {
      //create a new Response object
      $response = new Response();

      //set the return value
      $response->setContent('Hello World!');

      //make sure we send a 200 OK status
      $response->setStatusCode(Response::HTTP_OK);
      
      // set the response content type to plain text
      $response->headers->set('Content-Type', 'text/plain');
      
      // send the response with appropriate headers
      $response->send();
    }
}
?>
```

## 5. Crea un Endpoint de POST
Ve a la ruta

`/src/Controller/ Crea un archivo llamado ReverseController.php`

Y añadimos las siguientes lineas de codigo :

```
<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

// add request handling
use Symfony\Component\HttpFoundation\Request;

class ReverseController extends AbstractController
{
    //set the route, so [site URL]/hello will trigger this
    #[Route('/reverse', name: 'reverse_me')]
    public function reverse(): Response
    {          
      //create a request object to get request data
      $request = Request::createFromGlobals();

      //create a new Response object
      $response = new Response();

      // make sure the reverse_this parameter exists and is a string
      if ($request->request->get("reverse_this") && is_string($request->request->get("reverse_this"))) {

        //reverse the string and add it to the response 
        $response->setContent(strrev($request->request->get("reverse_this")));
        
        //make sure we send a 200 OK status
        $response->setStatusCode(Response::HTTP_OK);

      } else {

        //provide useful error message
        $response->setContent("The /reverse endpoint requires a POST with a 'reverse_this' parameter containing a text string.");

        //make sure we send an error status
        $response->setStatusCode(Response::HTTP_BAD_REQUEST);

      }

      // set the response content type to plain text
      $response->headers->set('Content-Type', 'text/plain');
      
      // send the response
      $response->send();
    }
}
?>
```

## 6. Enviar formulario desde controlador
Vamos a la ruta :

`src\Controller\BlogController.php`

Y añadimos el codigo :

```
<?php
use App\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Comment;
...

#[Route('/posts/{slug}', name: 'get_post', methods: ['GET'])]
public function getPost(Post $post): Response
{
    $form = $this->createForm(CommentType::class);
    return $this->render('blog/post.html.twig', [
        'post' => $post,
        'form' => $form->createView(),
    ]);
}

#[Route('/posts/{slug}', name: 'save_comment', methods: ['POST'])]
public function saveComment(Request $request, Post $post, EntityManagerInterface $entityManager): Response
{
    $comment = new Comment();
    $comment->setUser($this->getUser());
    $comment->setPost($post);
    $comment->setCreatedAt(new DateTimeImmutable());

    $form = $this->createForm(CommentType::class, $comment);
    $form->handleRequest(($request));

    if ($form->isSubmitted() && $form->isValid()) {
        $entityManager->persist($comment);
        $entityManager->flush();

        return $this->redirectToRoute('get_post', ['slug' => $post->getSlug()]);
    }
    return $this->render('blog/post.html.twig', [
        'post' => $post,
        'form' => $form->createView(),
    ]);
}
?>
```

---

Añadir POST tenemos que ir a la ruta config/routes.yamls y añadir

´´´ app_post:
    path: /post
    controller: App\Controller\DefaultController::post
´´´

Añadir el metodo post al controlador en src/Controller/DefaultController.php y añadiremos 
´´´
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function index(): Response
    {
        return new Response('¡Hola, mundo!');
    }

    public function post(Request $request): Response
    {
        $data = $request->request->get('data');

        return new Response('¡Hola, ' . $data . '!');
    }
} 
´´´
